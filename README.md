# opensearch-ilm-sandbox

## env-vars

load env-vars from file:

    for /F %A in (env\local.env) do SET %A
    for /F %A in (env\dev.env) do SET %A

## Run script: Build schema

First, **load env-vars** (from file, or manually)

build the schema on OpenSearch (index-template, policy and index with mapping):

    ./create-schema-os-2.5.0.sh esdev.ims-iot-dev.com


## Push data to indices (not required!)

First, **load env-vars** (from file, or manually)

run a program that pushed data into alias (created by script) ohads_audit:

Then, run the program:

    ts-node src\audit-push-data.ts


----

to set rollover to 10m:

    "rollover": {
       "min_index_age": "10m"
    }

when set rollover to 1h, and deletion after 10 hours it will look this way in Index Management console:

![app-screenshot](images/policy_managed_indices_local.png)

![app-screenshot](images/mngd_index_info_local_init.png)

after init phase (internal), you will see this ("Pending rollover of index"):

![app-screenshot](images/policy_mngd_indices_local_2.png)

![app-screenshot](images/mngd_index_info_local_2.png)

After the rollover, it will look this way:

![app-screenshot](images/policy_mngd_indices_local_3.png)

index name must have number in the end: something-*?. so if I put the date in the end without the number, rollover fails:

this works:

    AUDIT_INDEX=%3C${AUDIT_BASE}-%7Bnow%2Fd%7D-0001%3E

this does not work:

    AUDIT_INDEX=%3C${AUDIT_BASE}-%7Bnow%2Fd%7D%3E

![app-screenshot](images/rollover_failure_1.png)

