#!/bin/bash

if [ -z $1 ]
then
  ELASTIC=localhost:9210
  echo Using default localhost
else
  ELASTIC=$1
fi

# base
BASE=ohads
BASE_INDEX_NAME=${BASE}
echo "using BASE of $BASE, BASE_INDEX_NAME is $BASE_INDEX_NAME"

# genral pattern and template (for shards and replicas)
ME8_PATTERN=${BASE}_*
ME8_TEMPLATE=${BASE}_template

# audit
AUDIT_BASE=${BASE_INDEX_NAME}_audit
AUDIT_ALIAS=${BASE}_audit
AUDIT_PATTERN=${AUDIT_BASE}-*
AUDIT_POLICY=${AUDIT_BASE}_policy
AUDIT_TEMPLATE=${AUDIT_BASE}_template
# URI encoded index name for '<${AUDIT_BASE}-{now/d}-001>' - required for ES date math
AUDIT_INDEX=%3C${AUDIT_BASE}-%7Bnow%2Fd%7D-001%3E
#AUDIT_INDEX=%3C${AUDIT_BASE}-%7Bnow%2Fd%7D%3E

export HTTP=${OPENSEARCH_PROTOCOL:=https}
export HTTP_AUTH=${OPENSEARCH_USERNAME}:${OPENSEARCH_PASSWORD}
export ELASTIC=${HTTP_AUTH}@${ELASTIC}
CURL="curl -ku $HTTP_AUTH"

echo "elastic server $ELASTIC, audit index $AUDIT_INDEX"
echo "CURL=$CURL" 

create_shards_and_replicas_template() {
  echo -e "\n\nelastic server: <${1}>, template: <${2}>, index pattern: <${3}>, shards amount: <${4}>, replicas amount: <${5}>"
  echo "About to delete and recreate template ${2} which apply over index pattern ${3} to set ${4} shards amount and ${5} replicas amount!"

  if [ ${NON_INTERACTIVE} != "TRUE" ]; then
     read -p "Are you sure [yn]? " -n 1 -r
  else
     REPLY=n
  fi
  echo    # (optional) move to a new line
  if [[ ${REPLY} =~ ^[Yy]$ ]] || [ ${NON_INTERACTIVE} == "TRUE" ];
  then
      echo -e "\n\nDeleting ${2} template"
      $CURL -XDELETE ${HTTP}://${1}/_index_template/${2}

      echo -e "\n\nCreating ${2} template with ${3} pattern"
      $CURL -H 'Content-Type: application/json' -XPUT ${HTTP}://${1}/_index_template/${2} -d '
      {
         "index_patterns": ["'${3}'"],
         "priority": 0,
         "template": {
           "settings": {
              "number_of_shards": '${4}',
              "number_of_replicas": '${5}'
           }
         }
      }'      
  else
      echo "skipping creation of template ${2} on server ${1}"
  fi  
}


create_audit_ilm_policy() {
  echo -e "\n\nelastic server ${1}, policy ${2}, index pattern ${3}"
  echo "About to delete and recreate ilm policy ${2} for audit indices!"

   if [ ${NON_INTERACTIVE} != "TRUE" ]; then
     read -p "Are you sure [yn]? " -n 1 -r
   else
     REPLY=n
   fi
   echo    # (optional) move to a new line
   if [[ ${REPLY} =~ ^[Yy]$ ]] || [ ${NON_INTERACTIVE} == "TRUE" ];
   then
      echo -e "\n\nRemoving ${2} ilm policy from ${3} indices"
      $CURL -XPOST ${HTTP}://${1}/_plugins/_ism/remove/${3} -H 'Content-Type: application/json' -d '{}'

      echo -e "\n\nDeleting ${2} ilm policy"
      $CURL -XDELETE ${HTTP}://${1}/_plugins/_ism/policies/${2}

      echo -e "\n\nCreating ${2} ilm policy"
      $CURL -H 'Content-Type: application/json' -XPUT ${HTTP}://${1}/_plugins/_ism/policies/${2} -d '
      {
         "policy": {
            "description": "ohads policy",
            "default_state": "hot",
            "schema_version": 1,
            "states": [
               {
                  "name": "hot",
                  "actions": [
                     {
                        "rollover": {
                           "min_index_age": "1h"
                        }
                     }
                  ],
                  "transitions": [
                     {
                        "state_name": "delete",
                        "conditions": {
                           "min_index_age": "5h"
                        }
                     }
                  ]
               },               
               {
                  "name": "delete",
                  "actions": [
                     {
                        "delete": {}
                     }
                  ],
                  "transitions": []
               }
            ],
            "ism_template": {
               "index_patterns": ["'${3}'"],
               "priority": 100
            }
         }
      }'
   else
      echo "skipping creation of ilm policy ${2} on server ${1}"
   fi
}

create_audit_ilm_template() {
   echo -e "\n\nelastic server ${1}, template ${2}, index alias ${4}, index pattern ${5}"
   echo "About to delete and recreate ilm template ${2} for alias ${4} which apply over index pattern ${5}!"

   if [ ${NON_INTERACTIVE} != "TRUE" ]; then
      read -p "Are you sure [yn]? " -n 1 -r
   else
      REPLY=n
   fi
   
   echo    # (optional) move to a new line
   if [[ ${REPLY} =~ ^[Yy]$ ]] || [ ${NON_INTERACTIVE} == "TRUE" ];
   then
      echo -e "\n\nDeleting ${2} template"
      $CURL -XDELETE ${HTTP}://${1}/_index_template/${2}

      echo -e "\n\nCreating <${2}> template, with <${4}> alias & <${5}> pattern"
      $CURL -H 'Content-Type: application/json' -XPUT ${HTTP}://${1}/_index_template/${2} -d '
      {
         "index_patterns": ["'${5}'"],
         "priority": 1,
         "template": {
           "settings": {
              "index": {
                "opendistro.index_state_management.rollover_alias": "'${4}'",
                "mapping.coerce": false,
                "number_of_shards": 2,
                "number_of_replicas": 1
              }
           },
           "mappings": '"$(cat ./latest-mapping/audit_mapping.json)"'
         }
      }'

   else
      echo "skipping creation of ilm template ${2} on server ${1}"
   fi
}

create_audit_ilm_base_index() {   
   echo -e "\n\nelastic server ${1}, base index ${2} (URI encoded!), alias ${3}"
   echo "About to delete and recreate the index ${2} (URI encoded!)"

   if [ ${NON_INTERACTIVE} != "TRUE" ]; then
      read -p "Are you sure [yn]? " -n 1 -r
   else
      REPLY=n
   fi
   echo    # (optional) move to a new line
   if [[ ${REPLY} =~ ^[Yy]$ ]] || [ ${NON_INTERACTIVE} == "TRUE" ];
   then
      echo -e "\n\nDeleting index ${3} (if existed as an index), URL: <${HTTP}://${1}/${3}>"
      $CURL -XDELETE ${HTTP}://${1}/${3}      

      echo -e "\n\nDeleting all indices who are connected to ${3} alias), URL: <${HTTP}://${1}/_alias/${3}>"
      # echo -e "$($CURL -XGET ${HTTP}://${1}/_alias/${3} | jq -r 'keys[]')"
      INDICES_LIST="$($CURL -XGET ${HTTP}://${1}/_alias/${3} | jq -r 'keys[]')"
      echo -e ${INDICES_LIST}
      
      for INDEX in ${INDICES_LIST}
      do
         # remove carriage return \r which breaks the string:
         INDEX=$(echo ${INDEX} | tr -d '\r')
         echo -e "\n\n[create_audit_ilm_base_index] - Deleting index ${INDEX}, URL: <${HTTP}://${1}/${INDEX}>"
         $CURL -XDELETE ${HTTP}://${1}/${INDEX}
      done

      echo -e "\n\n[create_audit_ilm_base_index] - Creating base index <${2}> (URI encoded!) with <${3}> alias, URL: <${HTTP}://${1}/${2}>"
      $CURL -H 'Content-Type: application/json' -XPUT ${HTTP}://${1}/${2} -d '
      {
         "settings": {
           "index": {
              "refresh_interval": "60s"
           }
         },
         "aliases": {
            "'${3}'": {
                "is_write_index": true
            }
         }
      }'      

      echo -e "\n\nCreating audits mapping for index <${2}> (URI encoded!)!, URL: <${HTTP}://${1}/${2}/_mapping>"
      $CURL -H 'Content-Type: application/json' -XPUT ${HTTP}://${1}/${2}/_mapping -d '@./latest-mapping/audit_mapping.json'
   else
      echo "skipping creation of index ${2} (URI encoded!) on server ${1}"
   fi
}



if [ "${DEPLOYMENT_MODE}" == "INIT" ]; then
   echo "In init mode! setting NON_INTERACTIVE to TRUE"
   NON_INTERACTIVE=TRUE
else
   echo "Launching in non-interactive mode"
   NON_INTERACTIVE=FALSE
fi

create_shards_and_replicas_template $ELASTIC $ME8_TEMPLATE $ME8_PATTERN ${IOT_ELASTIC_SHARDS_AMOUNT} ${IOT_ELASTIC_REPLICAS_AMOUNT}

create_audit_ilm_policy ${ELASTIC} ${AUDIT_POLICY} ${AUDIT_PATTERN} ${AUDIT_TEMPLATE} ${AUDIT_ALIAS}
create_audit_ilm_template ${ELASTIC} ${AUDIT_TEMPLATE} dummy ${AUDIT_ALIAS} ${AUDIT_PATTERN}
create_audit_ilm_base_index ${ELASTIC} ${AUDIT_INDEX} ${AUDIT_ALIAS}

exit 0

