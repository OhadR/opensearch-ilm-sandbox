import { Client } from "@elastic/elasticsearch";
import * as crypto from "crypto";
import { getLogger } from "log4js";
const logger = getLogger('Migrator');
logger.level = 'info';

const TARGET_INDEX = 'ohads_audit';

class Migrator {
    protected _elasticClient: Client;


    constructor() {
        //connect to ES (prod-like)
        this._elasticClient = new Client({
            node: process.env.ES_URL,
            auth: {
                username: process.env.ES_USERNAME,
                password: process.env.ES_PASSWORD,
            }
        });
    }

    static sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    public async indexItemsToTarget() {
        let counter = 0;
        while(++counter) {
            await Migrator.sleep(10);

            await this._elasticClient.index({
                    refresh: true,
                    index: TARGET_INDEX,
                    id: Date.now().toString(),
                    body: {
                        audit_version: 4.4,
                        action_api: crypto.randomBytes(20).toString('hex'),
                        a: 'bb'}
                }
            );
            if(counter % 1000 === 0)
                logger.info(`${counter} items were indexed`);
        }
    }
}


/**
 * run before :
 * exportDotEnv local.env
 */
async function main() {
    const migrator = new Migrator();
    await migrator.indexItemsToTarget();
}

logger.info('start')
main();
