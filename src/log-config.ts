import { configure, getLogger } from "log4js";

configure({
    appenders: {
        out: { type: 'stdout', layout: {
                type: 'pattern',
                pattern: '%[[%d] [%p] %c %] %m',
            }}
    },
    categories: { default: { appenders: ['out'], level: 'info' } }
});

getLogger('log-config').info('logger is configured.');